#!/bin/sh -e

version=$(jq -r '.version' app/dns-server/nodejs/package.json)
docker-buildx create --use
docker-buildx build --platform linux/arm64,linux/amd64 app/dns-server -f app/dns-server/docker/nodejs/Dockerfile --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$version --tag $CI_REGISTRY_IMAGE:latest --push
